use schoolchoice;

CREATE table instituicao (
	instituicao_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nomeInstituicao VARCHAR(50) NOT NULL,
	cnpj VARCHAR(14) UNIQUE NOT NULL,
    telefone VARCHAR(20),
    email VARCHAR(50) NOT NULL,
    logradouro VARCHAR(20) NOT NULL,
    numero INTEGER NOT NULL,
    complemento VARCHAR(20),
    notaMec CHAR(1),
    dataAgendamento DATE NOT NULL,
    horaAgendamento TIME NOT NULL,
    enabled BIT
);