USE schoolchoice;

CREATE TABLE usuario (
	usuario_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    senha VARCHAR(15) NOT NULL,
    tipoUsuario CHAR(1) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    sobrenome VARCHAR(50) NOT NULL,
	cpf VARCHAR(11) UNIQUE NOT NULL,
    genero VARCHAR(20),
    telefone VARCHAR(20),
    dataNascimento TIMESTAMP,
    estadocivil VARCHAR(20),
    cep VARCHAR(20) NOT NULL,
    logradouro VARCHAR(20),
    numero INTEGER,
    complemento VARCHAR(20),
	enabled BIT
);