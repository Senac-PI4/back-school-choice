package br.senac.school.model.instituicoes;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;

//Classe de neg�cio de institui��o
public class Instituicao {
	
	//atributos
	private Integer instituicao_id;
	private String nomeInstituicao;
	private String cnpj;
	private String telefone;
	private String email;
	private String logradouro;
	private Integer numero;
	private String complemento;
	private Integer notaMec;
	private Date dataAgendamento;
	private LocalTime horaAgendamento;
	
	// M�todos de acesso
	
	public Integer getInstituicao_id() {
		return instituicao_id;
	}
	public void setInstituicao_id(Integer iinstituicao_id) {
		this.instituicao_id = iinstituicao_id;
	}
	public String getNomeInstituicao() {
		return nomeInstituicao;
	}
	public void setNomeInstituicao(String nomeInstituicao) {
		this.nomeInstituicao = nomeInstituicao;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public Integer getNotaMec() {
		return notaMec;
	}
	public void setNotaMec(Integer notaMac) {
		this.notaMec = notaMac;
	}
	public Date getDataAgendamento() {
		return dataAgendamento;
	}
	public void setDataAgendamento(Date dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}
	public LocalTime getHoraAgendamento() {
		return horaAgendamento;
	}
	public void setHoraAgendamento(LocalTime horaAgendamento) {
		this.horaAgendamento = horaAgendamento;
	}

}
