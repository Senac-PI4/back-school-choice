package br.senac.school.model.validador;

import br.senac.school.model.instituicoes.Instituicao;

public class ValidadorInstituicao {

	
	public static String validar(Instituicao instituicao) {
		
		if(instituicao.getNomeInstituicao() == null) {
			return "� preciso informar o nome da institui��o";
		}
		
		if(instituicao.getCnpj() == null || instituicao.getCnpj().length() < 14) {
			return "O cnpj informado � inv�lido";
		}
		
		if ((instituicao.getEmail().contains("@"))
                && (instituicao.getEmail().contains("."))) {

            String valor = new String(instituicao.getEmail().substring(0,
            		instituicao.getEmail().lastIndexOf('@')));

            String dominio = new String(instituicao.getEmail().substring(instituicao.getEmail().lastIndexOf('@') + 1, instituicao.getEmail().length()));

            if ((valor.length() >= 1) && (!valor.contains("@"))
                    && (dominio.contains(".")) && (!dominio.contains("@")) && (dominio.indexOf(".")
                    >= 1) && (dominio.lastIndexOf(".") < dominio.length() - 1)) {
            } else {

                return "E-mail Inv�lido";
            }

        } else {

            return "E-mail Inv�lido";

        }
		
		if(instituicao.getLogradouro() == null) {
			return "� preciso informar o logradouro";
		}
		
		if(instituicao.getNumero() == null) {
			return "� preciso informar o n�mero da institui��o";
		}
		
		if(instituicao.getDataAgendamento() == null) {
			return "� preciso informar a data de agendamento da visita";
		}
		
		if(instituicao.getHoraAgendamento() == null) {
			return "� preciso informar a hora de agendamento da visita";
		}
		return null;
	}
}
