package br.senac.school.model.validador;

import br.senac.school.model.usuarios.Usuario;

public class ValidadorUsuario {

	public static String validar(Usuario usuario) {
        //Realiza��o de valida��es de neg�cio
        //Verifica se foi especificado um usuario.
        if (usuario == null) {
            return "N�o foi informado um usuario";
        }

        //Verifica se o usuario tem nome definido.
        if (usuario.getNome() == null || "".equals(usuario.getNome())) {
            return "� necess�rio informar um nome de usuario";
        }

        //Verifica se o usuario tem sobrenome.
        if (usuario.getSobrenome() == null
                || "".equals(usuario.getSobrenome())) {
            return "� necess�rio informar um sobrenome de usuario";
        }

        //Verifica se o usuario tem data de nascimento.
        if (usuario.getDataNascimento() == null) {
            return "� necess�rio informar um valor de data de nascimento"
                    + " v�lido";
        }

        //Verifica se o usuario tem um g�nero e o mesmo � v�lido
        if (usuario.getGenero() == null || "".equals(usuario.getGenero())
                || (!usuario.getGenero().equals("Masculino"))
                && !usuario.getGenero().equals("Feminino")) {

            return "� necess�rio informar um g�nero v�lido para o usuario";

        }
        
        // Verifica se o estado civil foi informado.
        if(usuario.getEstadocivil() == null || "".equals(usuario.getEstadocivil())){
            return "� preciso informar o Estado Civil";
        }
        
        // Verifica se o cpf informado � v�lido.
        if(usuario.getCpf() == null || "".equals(usuario.getCpf()) || usuario.getCpf().length() > 11 || usuario.getCpf().length() < 11){ 
            return "O CPF informado � inv�lido";
        }
        // Verifica se o cpf informado � realmente v�lido/verdadeiro.
        if (usuario.getCpf() != null && usuario.getCpf() != "") {
            try {

                int d1, d4, xx, nCount, resto, digito1, digito2;
                String Check;
                String Separadores = "/-.";
                d1 = 0;
                d4 = 0;
                xx = 1;

                for (nCount = 0; nCount < usuario.getCpf().length() - 2; nCount++) {
                    String s_aux = usuario.getCpf().substring(nCount, nCount + 1);

                    if (Separadores.indexOf(s_aux) == -1) {
                        d1 = d1 + (11 - xx) * Integer.valueOf(s_aux).intValue();
                        d4 = d4 + (12 - xx) * Integer.valueOf(s_aux).intValue();
                        xx++;
                    }
                }

                resto = (d1 % 11);

                if (resto < 2) {
                    digito1 = 0;
                } else {
                    digito1 = 11 - resto;
                }

                d4 = d4 + 2 * digito1;
                resto = (d4 % 11);

                if (resto < 2) {
                    digito2 = 0;
                } else {
                    digito2 = 11 - resto;
                }

                Check = String.valueOf(digito1) + String.valueOf(digito2);

                String s_aux2 = usuario.getCpf().substring(usuario.getCpf().length() - 2, usuario.getCpf().length());

                if (s_aux2.compareTo(Check) != 0) {
                    return "O cpf informado � invalido";
                }

            } catch (Exception e) {
                return "O cpf informado � invalido";
            }
        }
        
        // Verificar se o CEP informado � livro.
        if(usuario.getCep() == null || usuario.getCep().length() < 8 || usuario.getCep().length() > 8 || "".equals(usuario.getCep())){
            return "� preciso informador um CEP v�lido";
        }
        
        // Verifica se o numero da resid�ncia foi informado.
        if(usuario.getNumero() == null || "".equals(usuario.getNumero()) || usuario.getNumero().toString().length() > 6){
            return "� preciso informar um n�mero da resid�ncia";
        }
        
        // Verifica se o logradouro foi informado.
        if(usuario.getLogradouro() == null || "".equals(usuario.getLogradouro())){
            return "� preciso informar o logradouro";
        }
        
        // Verifica se o complemento foi informado
        if(usuario.getComplemento() == null || "".equalsIgnoreCase(usuario.getComplemento())){
            return "� preciso informar um complemento";
        }
        
        if ((usuario.getEmail().contains("@"))
                && (usuario.getEmail().contains("."))) {

            String valor = new String(usuario.getEmail().substring(0,
                    usuario.getEmail().lastIndexOf('@')));

            String dominio = new String(usuario.getEmail().substring(usuario.getEmail().lastIndexOf('@') + 1, usuario.getEmail().length()));

            if ((valor.length() >= 1) && (!valor.contains("@"))
                    && (dominio.contains(".")) && (!dominio.contains("@")) && (dominio.indexOf(".")
                    >= 1) && (dominio.lastIndexOf(".") < dominio.length() - 1)) {
            } else {

                return "E-mail Inv�lido";
            }

        } else {

            return "E-mail Inv�lido";

        }
        
        // Verifica se o telefone informado est� nullo ou sem contem alguma letra
        //ou caracter especial
        if(usuario.getTelefone() == null || !usuario.getTelefone().matches("[0-9]*")){
            return "O telefone informado � inv�lido";
        }
        
        // Verifica se a senha informada est� com os param�tros corretos de seguran�a.
        if(usuario.getSenha() == null || usuario.getSenha().length() > 15 || usuario.getSenha().length() <5 ) {
        	return "� preciso informar uma senha v�lida, com m�nimo 6 e m�ximo 15 caracteres.";
        }
        
        if(usuario.getTipoUsuario() == null || usuario.getTipoUsuario() < 0 || usuario.getTipoUsuario() > 1) {
        	return "O tipo do usu�rio deve ser 0 ou 1.";
        }
        
        //Retorna "null" indicando que todas as valida��es foram feitas e o
        //usuario � um usuario v�lido de acordo com as necessidades do neg�cio
        return null;
    }
	
}
