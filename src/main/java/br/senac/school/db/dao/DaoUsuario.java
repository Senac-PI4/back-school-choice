package br.senac.school.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.senac.school.db.utils.ConnectionUtils;
import br.senac.school.model.usuarios.Usuario;

//Data Access Object de usuario. Realiza opera��es de BD com o usuario. 
public class DaoUsuario {

    //Insere um usuario na tabela "usuario" do banco de dados
    public static void inserir(Usuario usuario)
            throws SQLException, Exception {
    	
        //Monta a string de inser��o de um usuario no BD,
        //utilizando os dados do usuarios passados como par�metro
        String sql = "INSERT INTO usuario (email, senha, tipoUsuario, nome, sobrenome, cpf, genero, telefone, dataNascimento, estadocivil, cep, logradouro, numero,"
        		+ "complemento, enabled) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, usuario.getEmail());
            preparedStatement.setString(2, usuario.getSenha());
            preparedStatement.setInt(3, usuario.getTipoUsuario());
            preparedStatement.setString(4, usuario.getNome());
            preparedStatement.setString(5, usuario.getSobrenome());
            preparedStatement.setString(6, usuario.getCpf());
            preparedStatement.setString(7, usuario.getGenero());
            preparedStatement.setString(8, usuario.getTelefone());
            Timestamp t = new Timestamp(usuario.getDataNascimento().getTime());
            preparedStatement.setTimestamp(9, t);
            preparedStatement.setString(10, usuario.getEstadocivil());
            preparedStatement.setString(11, usuario.getCep());
            preparedStatement.setString(12, usuario.getLogradouro());
            preparedStatement.setInt(13, usuario.getNumero());
            preparedStatement.setString(14, usuario.getComplemento());
            preparedStatement.setBoolean(15, true);
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
            
        	//Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Realiza a atualiza��o dos dados de um usuario, com ID e dados
    //fornecidos como par�metro atrav�s de um objeto da classe "usuario"
    public static void atualizar(Usuario usuario)
            throws SQLException, Exception {
        
    	//Monta a string de atualiza��o do usuario no BD, utilizando
        //prepared statement
        String sql = "UPDATE usuario SET email=?, senha=?, tipoUsuario=?, nome=?, sobrenome=?, cpf=?, genero=?, telefone=?, dataNascimento=?,"
        		+ "estadocivil=?, cep=?, logradouro=?, numero=?, complemento=? "
            + "WHERE (usuario_id=?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, usuario.getEmail());
            preparedStatement.setString(2, usuario.getSenha());
            preparedStatement.setInt(3, usuario.getTipoUsuario());
            preparedStatement.setString(4, usuario.getNome());
            preparedStatement.setString(5, usuario.getSobrenome());
            preparedStatement.setString(6, usuario.getCpf());
            preparedStatement.setString(7, usuario.getGenero());
            preparedStatement.setString(8, usuario.getTelefone());
            Timestamp t = new Timestamp(usuario.getDataNascimento().getTime());
            preparedStatement.setTimestamp(9, t);
            preparedStatement.setString(10, usuario.getEstadocivil());
            preparedStatement.setString(11, usuario.getCep());
            preparedStatement.setString(12, usuario.getLogradouro());
            preparedStatement.setInt(13, usuario.getNumero());
            preparedStatement.setString(14, usuario.getComplemento());
            preparedStatement.setInt(15, usuario.getUsuario_id());
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
        	
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Realiza a exclus�o l�gica de um usuario no BD, com ID fornecido
    //como par�metro. A exclus�o l�gica simplesmente "desliga" o
    //usuario, configurando um atributo espec�fico, a ser ignorado
    //em todas as consultas de usuario ("enabled").
    public static void excluir(Integer id) throws SQLException, Exception {
    	
        //Monta a string de atualiza��o do usuario no BD, utilizando
        //prepared statement
        String sql = "UPDATE usuario SET enabled=? WHERE (usuario_id=?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setBoolean(1, false);
            preparedStatement.setInt(2, id);
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
        	
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Lista todos os usuarios da tabela usuarios
	public static List<Usuario> listar()
            throws SQLException, Exception {
    	
        //Monta a string de listagem de usuarios no banco, considerando
        //apenas a coluna de ativa��o de usuarios ("enabled")
        String sql = "SELECT * FROM usuario WHERE (enabled=?)";   
        
        //Lista de usuarios de resultado
        List<Usuario> listausuarios = null;
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Itera por cada item do resultado
            while (result.next()) {
            	
                //Se a lista n�o foi inicializada, a inicializa
                if (listausuarios == null) {
                    listausuarios = new ArrayList<Usuario>();
                }
                
                //Cria uma inst�ncia de usuario e popula com os valores do BD
                Usuario usuario = new Usuario();
                usuario.setUsuario_id(result.getInt("usuario_id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTipoUsuario(result.getInt("tipoUsuario"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setGenero(result.getString("genero"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setDataNascimento(result.getTimestamp("dataNascimento"));
                usuario.setEstadocivil(result.getString("estadocivil"));
                usuario.setCep(result.getString("cep"));
                usuario.setLogradouro(result.getString("logradouro"));
                usuario.setNumero(result.getInt("numero"));
                usuario.setComplemento(result.getString("complemento"));
                //Adiciona a inst�ncia na lista
                listausuarios.add(usuario);
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
        //Retorna a lista de usuarios do banco de dados
        return listausuarios;
        
    }

    //Procura um usuario no banco de dados, de acordo com o nome
    //ou com o sobrenome, passado como par�metro
    public static List<Usuario> procurar(String valor)
            throws SQLException, Exception {
    	
        //Monta a string de consulta de usuarios no banco, utilizando
        //o valor passado como par�metro para busca nas colunas de
        //nome ou sobrenome (atrav�s do "LIKE" e ignorando min�sculas
        //ou mai�sculas, atrav�s do "UPPER" aplicado � coluna e ao
        //par�metro). Al�m disso, tamb�m considera apenas os elementos
        //que possuem a coluna de ativa��o de usuarios configurada com
        //o valor correto ("enabled" com "true")
        String sql = "SELECT * FROM usuario WHERE ((UPPER(nome) LIKE UPPER(?) "
            + "OR UPPER(usuario.sobrenome) LIKE UPPER(?)) AND enabled=?)";
        
        //Lista de usuarios de resultado
        List<Usuario> listausuarios = null;
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, "%" + valor + "%");
            preparedStatement.setString(2, "%" + valor + "%");
            preparedStatement.setBoolean(3, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Itera por cada item do resultado
            while (result.next()) {
            	
                //Se a lista n�o foi inicializada, a inicializa
                if (listausuarios == null) {
                    listausuarios = new ArrayList<Usuario>();
                }
                
                //Cria uma inst�ncia de usuario e popula com os valores do BD
                Usuario usuario = new Usuario();
                usuario.setUsuario_id(result.getInt("usuario_id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTipoUsuario(result.getInt("tipoUsuario"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setGenero(result.getString("genero"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setDataNascimento(result.getTimestamp("dataNascimento"));
                usuario.setEstadocivil(result.getString("estadocivil"));
                usuario.setCep(result.getString("cep"));
                usuario.setLogradouro(result.getString("logradouro"));
                usuario.setNumero(result.getInt("numero"));
                usuario.setComplemento(result.getString("complemento"));
                
                //Adiciona a inst�ncia na lista
                listausuarios.add(usuario);
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
        //Retorna a lista de usuarios do banco de dados
        return listausuarios;
        
    }

    //Obt�m uma inst�ncia da classe "usuario" atrav�s de dados do
    //banco de dados, de acordo com o ID fornecido como par�metro
    public static Usuario obter(Integer id)
            throws SQLException, Exception {
    	
        //Comp�e uma String de consulta que considera apenas o usuario
        //com o ID informado e que esteja ativo ("enabled" com "true")
        String sql = "SELECT * FROM usuario WHERE (usuario_id=? AND enabled=?)";

        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setInt(1, id);            
            preparedStatement.setBoolean(2, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Verifica se h� pelo menos um resultado
            if (result.next()) {
            	
                //Cria uma inst�ncia de usuario e popula com os valores do BD
                Usuario usuario = new Usuario();
                usuario.setUsuario_id(result.getInt("usuario_id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setTipoUsuario(result.getInt("tipoUsuario"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setGenero(result.getString("genero"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setDataNascimento(result.getTimestamp("dataNascimento"));
                usuario.setEstadocivil(result.getString("estadocivil"));
                usuario.setCep(result.getString("cep"));
                usuario.setLogradouro(result.getString("logradouro"));
                usuario.setNumero(result.getInt("numero"));
                usuario.setComplemento(result.getString("complemento"));
                
                //Retorna o resultado
                return usuario;
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }

        //Se chegamos aqui, o "return" anterior n�o foi executado porque
        //a pesquisa n�o teve resultados
        //Neste caso, n�o h� um elemento a retornar, ent�o retornamos "null"
        return null;
        
    }
    
}