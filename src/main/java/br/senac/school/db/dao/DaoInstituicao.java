package br.senac.school.db.dao;

import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.senac.school.db.utils.ConnectionUtils;
import br.senac.school.model.instituicoes.Instituicao;

public class DaoInstituicao {

	//Insere um instituicao na tabela "instituicao" do banco de dados
    public static void inserir(Instituicao instituicao)
            throws SQLException, Exception {
    	
        //Monta a string de inser��o de um instituicao no BD,
        //utilizando os dados do instituicaos passados como par�metro
        String sql = "INSERT INTO instituicao (nomeInstituicao, cnpj, telefone, email, logradouro, numero, complemento, notaMec,"
        		+ "dataAgendamento, horaAgendamento, "
        		+ "enabled) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, instituicao.getNomeInstituicao());
            preparedStatement.setString(2, instituicao.getCnpj());
            preparedStatement.setString(3, instituicao.getTelefone());
            preparedStatement.setString(4, instituicao.getEmail());
            preparedStatement.setString(5, instituicao.getLogradouro());
            preparedStatement.setInt(6, instituicao.getNumero());
            preparedStatement.setString(7, instituicao.getComplemento());
            preparedStatement.setInt(8, instituicao.getNotaMec());
            Timestamp t = new Timestamp(instituicao.getDataAgendamento().getTime());
            preparedStatement.setTimestamp(9, t);
            preparedStatement.setObject(10, instituicao.getHoraAgendamento(), JDBCType.TIME);
            preparedStatement.setBoolean(11, true);
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
            
        	//Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Realiza a atualiza��o dos dados de um instituicao, com ID e dados
    //fornecidos como par�metro atrav�s de um objeto da classe "instituicao"
    public static void atualizar(Instituicao instituicao)
            throws SQLException, Exception {
        
    	//Monta a string de atualiza��o do instituicao no BD, utilizando
        //prepared statement
        String sql = "UPDATE instituicao SET nomeInstituicao=?, cnpj=?, telefone=?, email=?, logradouro=?, numero=?,"
        		+ "complemento=?, notaMec=?, dataAgendamento=?, horaAgendamento=?"
            + "WHERE (instituicao_id=?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, instituicao.getNomeInstituicao());
            preparedStatement.setString(2, instituicao.getCnpj());
            preparedStatement.setString(3, instituicao.getTelefone());
            preparedStatement.setString(4, instituicao.getEmail());
            preparedStatement.setString(5, instituicao.getLogradouro());
            preparedStatement.setInt(6, instituicao.getNumero());
            preparedStatement.setString(7, instituicao.getComplemento());
            preparedStatement.setInt(8, instituicao.getNotaMec());
            Timestamp t = new Timestamp(instituicao.getDataAgendamento().getTime());
            preparedStatement.setTimestamp(9, t);
            preparedStatement.setObject(10, instituicao.getHoraAgendamento(), JDBCType.TIME);
            preparedStatement.setInt(11, instituicao.getInstituicao_id());
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
        	
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Realiza a exclus�o l�gica de um instituicao no BD, com ID fornecido
    //como par�metro. A exclus�o l�gica simplesmente "desliga" o
    //instituicao, configurando um atributo espec�fico, a ser ignorado
    //em todas as consultas de instituicao ("enabled").
    public static void excluir(Integer id) throws SQLException, Exception {
    	
        //Monta a string de atualiza��o do instituicao no BD, utilizando
        //prepared statement
        String sql = "UPDATE instituicao SET enabled=? WHERE (instituicao_id=?)";
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setBoolean(1, false);
            preparedStatement.setInt(2, id);
            
            //Executa o comando no banco de dados
            preparedStatement.execute();
            
        } finally {
        	
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
    }

    //Lista todos os instituicaos da tabela instituicaos
	public static List<Instituicao> listar()
            throws SQLException, Exception {
    	
        //Monta a string de listagem de instituicaos no banco, considerando
        //apenas a coluna de ativa��o de instituicaos ("enabled")
        String sql = "SELECT * FROM instituicao WHERE (enabled=?)";   
        
        //Lista de instituicaos de resultado
        List<Instituicao> listainstituicaos = null;
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Itera por cada item do resultado
            while (result.next()) {
            	
                //Se a lista n�o foi inicializada, a inicializa
                if (listainstituicaos == null) {
                    listainstituicaos = new ArrayList<Instituicao>();
                }
                
                //Cria uma inst�ncia de instituicao e popula com os valores do BD
                Instituicao instituicao = new Instituicao();
                instituicao.setInstituicao_id(result.getInt("instituicao_id"));
                instituicao.setNomeInstituicao(result.getString("nomeInstituicao"));
                instituicao.setCnpj(result.getString("cnpj"));
                instituicao.setTelefone(result.getString("telefone"));
                instituicao.setEmail(result.getString("email"));
                instituicao.setLogradouro(result.getString("logradouro"));
                instituicao.setNumero(result.getInt("numero"));
                instituicao.setComplemento(result.getString("complemento"));
                instituicao.setNotaMec(result.getInt("notaMec"));
                instituicao.setDataAgendamento(result.getTimestamp("dataAgendamento"));
                instituicao.setHoraAgendamento(result.getTime("horaAgendamento").toLocalTime());
                
                //Adiciona a inst�ncia na lista
                listainstituicaos.add(instituicao);
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
        //Retorna a lista de instituicaos do banco de dados
        return listainstituicaos;
        
    }

    //Procura um instituicao no banco de dados, de acordo com o nome
    //ou com o sobrenome, passado como par�metro
    public static List<Instituicao> procurar(String valor)
            throws SQLException, Exception {
    	
        //Monta a string de consulta de instituicaos no banco, utilizando
        //o valor passado como par�metro para busca nas colunas de
        //nome ou sobrenome (atrav�s do "LIKE" e ignorando min�sculas
        //ou mai�sculas, atrav�s do "UPPER" aplicado � coluna e ao
        //par�metro). Al�m disso, tamb�m considera apenas os elementos
        //que possuem a coluna de ativa��o de instituicaos configurada com
        //o valor correto ("enabled" com "true")
        String sql = "SELECT * FROM instituicao WHERE ((UPPER(nome) LIKE UPPER(?) "
            + "OR UPPER(instituicao.sobrenome) LIKE UPPER(?)) AND enabled=?)";
        
        //Lista de instituicaos de resultado
        List<Instituicao> listainstituicaos = null;
        
        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
        	
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setString(1, "%" + valor + "%");
            preparedStatement.setString(2, "%" + valor + "%");
            preparedStatement.setBoolean(3, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Itera por cada item do resultado
            while (result.next()) {
            	
                //Se a lista n�o foi inicializada, a inicializa
                if (listainstituicaos == null) {
                    listainstituicaos = new ArrayList<Instituicao>();
                }
                
                //Cria uma inst�ncia de instituicao e popula com os valores do BD
                Instituicao instituicao = new Instituicao();
                instituicao.setInstituicao_id(result.getInt("instituicao_id"));
                instituicao.setNomeInstituicao(result.getString("nomeInstituicao"));
                instituicao.setCnpj(result.getString("cnpj"));
                instituicao.setTelefone(result.getString("telefone"));
                instituicao.setEmail(result.getString("email"));
                instituicao.setLogradouro(result.getString("logradouro"));
                instituicao.setNumero(result.getInt("numero"));
                instituicao.setComplemento(result.getString("complemento"));
                instituicao.setNotaMec(result.getInt("notaMec"));
                instituicao.setDataAgendamento(result.getTimestamp("dataAgendamento"));
                instituicao.setHoraAgendamento(result.getTime("horaAgendamento").toLocalTime());
                
                //Adiciona a inst�ncia na lista
                listainstituicaos.add(instituicao);
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }
        
        //Retorna a lista de instituicaos do banco de dados
        return listainstituicaos;
        
    }

    //Obt�m uma inst�ncia da classe "instituicao" atrav�s de dados do
    //banco de dados, de acordo com o ID fornecido como par�metro
    public static Instituicao obter(Integer id)
            throws SQLException, Exception {
    	
        //Comp�e uma String de consulta que considera apenas o instituicao
        //com o ID informado e que esteja ativo ("enabled" com "true")
        String sql = "SELECT * FROM instituicao WHERE (instituicao_id=? AND enabled=?)";

        //Conex�o para abertura e fechamento
        Connection connection = null;
        //Statement para obten��o atrav�s da conex�o, execu��o de
        //comandos SQL e fechamentos
        PreparedStatement preparedStatement = null;
        //Armazenar� os resultados do banco de dados
        ResultSet result = null;
        
        try {
            //Abre uma conex�o com o banco de dados
            connection = ConnectionUtils.getConnection();
            
            //Cria um statement para execu��o de instru��es SQL
            preparedStatement = connection.prepareStatement(sql);
            
            //Configura os par�metros do "PreparedStatement"
            preparedStatement.setInt(1, id);            
            preparedStatement.setBoolean(2, true);
            
            //Executa a consulta SQL no banco de dados
            result = preparedStatement.executeQuery();
            
            //Verifica se h� pelo menos um resultado
            if (result.next()) {
            	
                //Cria uma inst�ncia de instituicao e popula com os valores do BD
                Instituicao instituicao = new Instituicao();
                instituicao.setInstituicao_id(result.getInt("instituicao_id"));
                instituicao.setNomeInstituicao(result.getString("nomeInstituicao"));
                instituicao.setCnpj(result.getString("cnpj"));
                instituicao.setTelefone(result.getString("telefone"));
                instituicao.setEmail(result.getString("email"));
                instituicao.setLogradouro(result.getString("logradouro"));
                instituicao.setNumero(result.getInt("numero"));
                instituicao.setComplemento(result.getString("complemento"));
                instituicao.setNotaMec(result.getInt("notaMec"));
                instituicao.setDataAgendamento(result.getTimestamp("dataAgendamento"));
                instituicao.setHoraAgendamento(result.getTime("horaAgendamento").toLocalTime());
                
                //Retorna o resultado
                return instituicao;
                
            }
            
        } finally {
        	
            //Se o result ainda estiver aberto, realiza seu fechamento
            if (result != null && !result.isClosed()) {
                result.close();
            }
            
            //Se o statement ainda estiver aberto, realiza seu fechamento
            if (preparedStatement != null && !preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            
            //Se a conex�o ainda estiver aberta, realiza seu fechamento
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            
        }

        //Se chegamos aqui, o "return" anterior n�o foi executado porque
        //a pesquisa n�o teve resultados
        //Neste caso, n�o h� um elemento a retornar, ent�o retornamos "null"
        return null;
        
    }
	
}
