package br.senac.school.servicos;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.senac.school.db.dao.DaoInstituicao;
import br.senac.school.model.instituicoes.Instituicao;
import br.senac.school.model.validador.ValidadorInstituicao;

@Path("/instituicao")
public class ServicoInstituicao {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirInstituicao(Instituicao instituicao) {
		Response response = null;
		try {
			if(ValidadorInstituicao.validar(instituicao) == null) {
				DaoInstituicao.inserir(instituicao);
				response = Response.status(Response.Status.OK.getStatusCode()).entity("Cadastro realizado com sucesso!")
						.build();
			}else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity(ValidadorInstituicao.validar(instituicao))
						.build();
			}
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarInstituicoes() {
		Response response = null;
		try {
			response = Response.status(Response.Status.OK.getStatusCode()).entity(DaoInstituicao.listar())
					.build();
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity("Erro ao realizar a listagem: " + e.getMessage())
					.build();
			e.printStackTrace();
		}
		return response;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response atualizarInstituicao(Instituicao instituicao) {
		Response response  =  null;
		try {
			if(ValidadorInstituicao.validar(instituicao) == null) {
				DaoInstituicao.atualizar(instituicao);
				response = Response.status(Response.Status.OK.getStatusCode()).entity("Atualiza��o realizada com sucesso!")
						.build();
			}else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity(ValidadorInstituicao.validar(instituicao))
						.build();
			}
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response removerInstituicao(@PathParam("id") Integer id) {
		Response response = null;
		try {
			if(id != null && id >= 0) {
				DaoInstituicao.excluir(id);	
				response = Response.status(Response.Status.OK.getStatusCode()).entity("Exclus�o realizada com sucesso!")
						.build();
			}else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity("� preciso informar o ID para excluir.")
						.build();
			}
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
	
}
