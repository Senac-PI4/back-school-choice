package br.senac.school.servicos;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.senac.school.db.dao.DaoUsuario;
import br.senac.school.model.usuarios.Usuario;
import br.senac.school.model.validador.ValidadorUsuario;

@Path("/usuario")
public class ServicoUsuario {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirUsuario(Usuario usuario) {
		Response response = null;
		try {
			if(ValidadorUsuario.validar(usuario) == null) {
				DaoUsuario.inserir(usuario);
				response = Response.status(Response.Status.OK.getStatusCode()).entity("")
						.build();
			}else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity(ValidadorUsuario.validar(usuario))
						.build();
			}
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarUsuarios() {
		Response response = null;
		try {
			response = Response.status(Response.Status.OK.getStatusCode()).entity(DaoUsuario.listar())
					.build();
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity("Erro ao realizar a listagem: " + e.getMessage())
					.build();
			e.printStackTrace();
		}
		return response;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response atualizarUsuario(Usuario usuario) {
		Response response  =  null;
		try {
			if(usuario.getUsuario_id() == null) {
				return response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity("� preciso informar o ID do usu�rio.")
						.build();
			}
			if(ValidadorUsuario.validar(usuario) == null) {
				DaoUsuario.atualizar(usuario);
				response = Response.status(Response.Status.OK.getStatusCode()).entity("")
						.build();
			}else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode()).entity(ValidadorUsuario.validar(usuario))
						.build();
			}
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response removerUsuario(@PathParam("id") Integer id) {
		Response response = null;
		try {
				DaoUsuario.excluir(id);
		} catch (Exception e) {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
					.entity("Ocorreu um erro ao tratar a requisi��o: " + e.getMessage()).build();
			e.printStackTrace();
		}
		return response;
	}
}
